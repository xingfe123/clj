(ns my-project.core
  (:gen-class)
  (:use clojure.core.matrix)
  (:require [clatrix.core :as cl]
            [clojure.core.matrix.operators :as M])
  (:refer-clojure :exclude [+ - *]))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
(defn rand-list
  [len epsilon]
  (map (fn [x] (cl/- (rand (cl/* 2 epsilon))
                  epsilon))
       (range 0 len)))

(def sample-data [[0 0 0]
                  [0 1 1]
                  [1 0 1]
                  [1 1 0]])
(defn eq-mat [A B]
  (and (= (count A) (count B))
       (reduce #(and %1 %2) (map = A B))))
(defn add-mat [A B]
  (mapv #(mapv + %1 %2) A B))

(defn plot-scatter []
  (view linear-samp-scatter))
(defn plot-model []
  (view (add-lines samp-scatter-plot
                   X (:fitted linear-samp-scatter))))
;(plot-model)
(defn gradient-descent [F' x-start step]
  (loop [x-old x-start]
    (let [x-new (- x-old (* step (F' x-old)))
          dx (- x-new x-old)]
      (if (< dx gradient-descent-precision)
        x-new (recur x-new)))))
(defn predict [coefs X]
  {:pre [(= (count coefs)
            (+ 1 (count X)))]}
  (let [X-with (conj X 1)
        products (map * coefs X-with)]
    (reduce + products)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn square-mat [n e]
  "creates a sequre matrix of size n*n whose elements are all e. "
  (let [ rep #(repeat n %)]
    (matrix (-> e rep rep))))
(defn id-mat [n]
  (let [ init (square-mat :clatrix n 0)
        ident (fn [ i j n]
                (if (= i j) 1 b ))]
    (cl/map-indexed indet init)))
(defn rand-mat [n]
  (matrix (repeat n ( repeat n (rand-int 100)))))
(defn rand-square-clmat [n]
  (cl/map rand-int (square-mat :clatrix n 100)))
(defn rand-square-mat [ n]
  (matrix (repeatedly n #(map rand-int (repeat n 100)))))
(defn id-computed-mat [n]
  (compute-matrix [ n n] #(if (= %1 %2) 1 0)))
(defn rand-computed-mat [ n m]
  (compute-matrix [n m]
                  (fn [ i j]
                    (rand-int 100))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn rand-in-range [min max]
  (let [len (- max min)
        rand-l (rant-int len)]
    (+ min rand-len)))
(defn make-sea-bass []
  (vector :sea-bass
          (rand-in-range 6 10)))
(defn make-salmon []
  )
(defn train-byes-classifier []
  (dataset-set-class fish-dataset 0)
  (classifier-train bayes-classifier fish-dataset))
(defn train-KL-classifier []
  ())

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
